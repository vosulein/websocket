﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebApiWebsocketClient.Controllers
{
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [Route("api/sendmessage"), HttpGet]
        public async Task<ActionResult<string>> Get(string message)
        {
            var clt = new ClientWebSocket();

            await clt.ConnectAsync(new Uri("ws://localhost:5000/ws"), CancellationToken.None);

            var superObj = new {SuperField = 1, SuperFieldMessage = "Hi"};

            var jsonObj = JsonConvert.SerializeObject(superObj);
            
            var bytes = Encoding.UTF8.GetBytes(jsonObj);

            await clt.SendAsync(new ArraySegment<byte>(bytes), WebSocketMessageType.Text, true, CancellationToken.None);
            
            return "value";
        }
    }
}